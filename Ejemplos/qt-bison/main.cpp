#include <iostream>
#include <sys/stat.h>
#include <fstream>
#include <string.h>

#include "nodo.h"
#include "scanner.h"
#include "parser.h"

extern int yyparse();
extern Nodo *raiz;
using namespace std;

int recorrerArbol(Nodo*);
void leerEntrada(char*);



enum Choice
{
    OPSUMA = 1,
    OPRESTA = 2,
    OPMULTIPLICAR = 3,
    OPDIVIDIR = 4,
    OPNUMERO = 5,

};


int main()
{
    while(true){
        char input[500];
        printf(">> ");
        fgets(input,sizeof (input),stdin);
        leerEntrada(input);
        memset(input,0,400);
    }
}

void leerEntrada(char entrada[400]){
    YY_BUFFER_STATE buffer = yy_scan_string(entrada);
    if(yyparse() == 0){
        int resultado = recorrerArbol(raiz);
        cout << "Resultado: " << resultado << endl;
     }else{
       cout << "Hubo un error" << endl;
     }


}


int recorrerArbol(Nodo* raiz){
    switch (raiz->tipo_) {
    case OPNUMERO:
    {
        return raiz->valor.toInt();
        break;
    }

    case OPSUMA:
    {
        Nodo izq = raiz->hijos.at(0);
        Nodo der = raiz->hijos.at(1);
        int a = recorrerArbol(&izq);
        int b = recorrerArbol(&der);
        return a + b;
        break;
    }

    case OPRESTA:
    {
        Nodo izq = raiz->hijos.at(0);
        Nodo der = raiz->hijos.at(1);
        int a = recorrerArbol(&izq);
        int b = recorrerArbol(&der);
        return a - b;
        break;
    }


    case OPMULTIPLICAR:
    {
        Nodo izq = raiz->hijos.at(0);
        Nodo der = raiz->hijos.at(1);
        int a = recorrerArbol(&izq);
        int b = recorrerArbol(&der);
        return a * b;
        break;
    }


    case OPDIVIDIR:
    {
        Nodo izq = raiz->hijos.at(0);
        Nodo der = raiz->hijos.at(1);
        int a = recorrerArbol(&izq);
        int b = recorrerArbol(&der);
        return a / b;
        break;
    }


    default:
    {
        cout << "No lo se"<<endl;
        return 0;
        break;
    }

    }
}
