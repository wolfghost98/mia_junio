

%{
	#include <iostream>
	#include "parser.h"	
    Nodo *raiz;
	
%}

%option case-insensitive
%option noyywrap


DIGITO [0-9]
NUMERO {DIGITO}


%%


 /*********************
  * Reglas y Acciones *
 **********************/

{NUMERO} {strcpy(yylval.text, yytext); return NUMERO;}
"(" 	{return(PARA);}
")" 	{return(PARC);}
"+" 	{return(SUMA);}
"-" 	{return(RESTA);}
"*" 	{return(MULTPLICAR);}
"/" 	{return(DIVIDIR);}
[\t\r\f] {}
" "		{}
 
.            {std::cout <<yytext <<" Error Lexico" << std::endl;}
%%




