#include "nodo.h"

Nodo::Nodo(QString t, QString v){
    tipo =t;
    valor = v;
    tipo_ = getTipo();
    hijos = QList<Nodo>();
}

int Nodo::getTipo()
{
    if(this->tipo=="SUMA")return 1;
    if(this->tipo=="RESTA")return 2;
    if(this->tipo=="MULTIPLICAR")return 3;
    if(this->tipo=="DIVIDIR")return 4;
    if(this->tipo=="NUMERO")return 5;
    return 0;
}

void Nodo::add(Nodo n){
    hijos.append(n);
}
