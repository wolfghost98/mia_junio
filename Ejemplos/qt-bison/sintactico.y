%{

/********************** 
 * Declaraciones en C *
 **********************/
  #include <iostream>
  #include "nodo.h"

    extern int yylex(void);
    extern Nodo *raiz;

    int yyerror(const char* mens){
        std::cout<<mens<<std::endl;
        return 0;
    }

%}
%error-verbose
/*************************
  Declaraciones de Bison *
 *************************/


%union
{
        char text[400];
        class Nodo *nodito;
}


%start inicio;


%token <text> NUMERO
%token <text>PARA
%token <text>PARC
%token <text>SUMA
%token <text>RESTA
%token <text>MULTPLICAR
%token <text>DIVIDIR


%type<nodito> exp;
%type<nodito> inicio;


/*Definir precedencia de operaciones*/
%left SUMA RESTA
%left MULTPLICAR DIVIDIR







%%
/***********************
 * Reglas Gramaticales *
 ***********************/

inicio: exp { raiz = $$;};



exp : exp SUMA exp {$$ = new Nodo("SUMA",""); $$->add(*$1); $$->add(*$3); };
    | exp RESTA exp {$$ = new Nodo("RESTA",""); $$->add(*$1); $$->add(*$3); };
    | exp MULTPLICAR exp {$$ = new Nodo("MULTIPLICAR",""); $$->add(*$1); $$->add(*$3); };
    | exp DIVIDIR exp {$$ = new Nodo("DIVIDIR",""); $$->add(*$1); $$->add(*$3); };
    | NUMERO { $$ = new Nodo("NUMERO",$1); };

%%
/**********************
 * Codigo C Adicional *
 **********************/
